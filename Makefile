#CROSS_COMPILE := aarch64-linux-gnu-
CROSS_COMPILE := arm-linux-gnueabihf-
ARCH:= arm
CC:=$(CROSS_COMPILE)gcc
LD:=$(CROSS_COMPILE)ld
objects =  main.o gpioconfig.o
rg351_gpio : $(objects)
	$(CC) -o rg351_gpio $(objects)
$(objects) :
gpioconfig.o : gpioconfig.c
main.o : main.c

clean :
	rm rg351_gpio $(objects)
upload :
	scp rg351_gpio root@192.168.2.243:/emuelec/bin/