#include "gpioconfig.h"

int keepgoing = 1;

// Callback called when SIGINT is sent to the process (Ctrl-C)
void signal_handler(int sig)
{
    printf("Ctrl-C pressed, cleaning up and exiting..\n");
    keepgoing = 0;
}

int main(int argc, char **argv)
{
    /* initial GPIO*/
    int gpio1 = 64;
    int gpio2 = 65;
    int value = 0;
    int nfds = 0;
    int gpio_fd1, gpio_fd2, timeout;
    struct epoll_event evd1;
    struct epoll_event evd2;
    struct epoll_event *events;
    char buf[MAX_BUF];
    int len;
    int cnt = 0;
    unsigned int valueback = 0;

    signal(SIGINT, signal_handler);

    gpio_export(gpio1);
    gpio_set_dir(gpio1, "in");
    gpio_set_edge(gpio1, "rising");
    printf("\nSucessfully Configure GPIO-%d as input\n", gpio1);

    gpio_export(gpio2);
    gpio_set_dir(gpio2, "in");
    gpio_set_edge(gpio2, "rising");
    printf("\nSucessfully Configure GPIO-%d as input\n", gpio2);
    gpio_fd1 = gpio_fd_open(gpio1, O_RDONLY);
    gpio_fd2 = gpio_fd_open(gpio2, O_RDONLY);
    timeout = POLL_TIMEOUT;

    int epollfd = epoll_create(10);
    events = calloc(10, sizeof(struct epoll_event));
    evd1.data.fd = gpio_fd1;
    evd1.events = EPOLLPRI;

    evd2.data.fd = gpio_fd2;
    evd2.events = EPOLLPRI;
    epoll_ctl(epollfd, EPOLL_CTL_ADD, gpio_fd1, &evd1);
    epoll_ctl(epollfd, EPOLL_CTL_ADD, gpio_fd2, &evd2);

    while (keepgoing)
    {
        nfds = epoll_wait(epollfd, events, 10, -1);
        if (nfds < 0)
        {
            printf("\npoll() failed!\n");
            return -1;
        }
        for (int i = 0; i < nfds; i++)
        {
            if (events->events & EPOLLPRI)
            {
                memset(buf, 0x00, sizeof(buf));
                len = read(events->data.fd, buf, sizeof(buf));
                lseek(events->data.fd, 0, SEEK_SET);
                if (cnt > 2)
                {
                    usleep(20000);
                    if (events->data.fd == gpio_fd1)
                    {
                        gpio_get_value(gpio1, &valueback);

                        if (valueback == 1)
                        {
                            printf("\nButton 1 pressed times X%d\n", cnt);
                            system("/emuelec/scripts/odroidgoa_utils.sh vol +");
                        }
                    }
                    else if (events->data.fd == gpio_fd2)
                    {
                        gpio_get_value(gpio2, &valueback);

                        if (valueback == 1)
                        {
                            printf("\nButton 2 pressed times X%d\n", cnt);
                            system("/emuelec/scripts/odroidgoa_utils.sh vol -");
                        }
                    }
                }
                cnt++;
            }
        }
    }
    gpio_fd_close(gpio_fd1);
    gpio_fd_close(gpio_fd2);
    return (0);
}
